# Notes for CVM heads presentation on Citation Index and ORCID

- Libraries can help with citations to works
- We've been maintaining citations for over 12 years
- Web of Science Harvesting
- Looking to expand our reach and offer new services
- What's the service looks like
- register your ORCID iD with the Libraries and get access to the service
- I'll get back to ORCID in a bit
- start with past 3 years
- what we ask. sustainability
- caveats
  - only finally published articles. need to be able to be accessed. no submitted or pending.
  - Will only be as complete as the data we get.


## Paula Cray
- Before: 4 works; 2 open access. All with DOIs. 2 open access. All from 2015-2016.
- After: 199 works; 64 open access. 168 DOIs. 64 open access. 1983-2019

- show units pages
- We're especially looking to support how to set up a profile for auto-updates and how to import existing citations into ORCID to have a fuller profile.

## People There & Possible ORCID iDs
- Paul Lunn (saved 114 already) https://orcid.org/0000-0002-7388-9766
- Barbara Sherry (saved 12 already)  https://orcid.org/0000-0001-9858-6094
- Lizette Hardie (saved 3 already)
- Paula Cray
