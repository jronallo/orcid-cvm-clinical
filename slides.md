title: ORCID and CVM DoCS
class: animation-fade
layout: true

.bottom-bar[
  https://ronallo.com/presentations/orcid-cvm-docs
]

---
class: impact
# ORCID and<br>CVM DoCS

## https://ronallo.com/presentations/orcid-cvm-docs
## Press "p" to see speaker notes.

???
If you'd like, these slides have speaker notes on them.

---

# Jason Ronallo
## NCSU Libraries
## Department Head of Digital Library Initiatives

jnronall@ncsu.edu

https://www.lib.ncsu.edu/staff/jnronall

???
We work with a range of different technologies to provide novel solutions to library and archives challenges.

Increasingly we're looking for how to support the research mission of the university.

---
class: impact, orcid
# ORCID

???
One of the reasons I'm here is to talk about ORCID. What it can do for you and what the Libraries can do to help you use ORCID.

---
# ORCID iD

https://orcid.org/0000-0003-1602-3461<br>
Amy Stewart

- A persistent digital **identifier** that distinguishes you from other researchers. Disambiguates names.
- A **record** that supports automatic links among all of your professional activities that ensures they are correctly attributed to you.
- Your iD is **yours** throughout your career, no matter where you work, who funds you, whether your name or field of research changes, or if your name appears in different forms in different places.
- Integrates with many different systems with its open API.

---
class: img

![](./images/munana-orcid.png)

???
Here's what an ORCID record page looks like. The interface isn't much, but I'll explain in a bit why ORCID is so useful and why the Libraries has developed services to encourage the use of it.

---
# Actions

1. Register with ORCID iD at: https://ci.lib.ncsu.edu
2. Send us your CV: https://ci.lib.ncsu.edu/user/cv-service
3. Use your ORCID iD when you publish.

???
Before I get into more details, I want to start at the end.

Why I'm really here is that I'm hoping you will take 3 actions. The first two can be completed while I'm talking. I won't be offended if you just go ahead and start now.

I'm hoping that more of you will register your ORCID iD with the Libraries so that we can help save you time.

You can get started by visiting ci dot lib dot ncsu dot edu.

Then here's the link so you can send us your CV or another list of your publications.

---
class: impact
# Why ORCID?
## ORCID Use Cases

???
So just a couple of other reasons why we encourage researchers to use ORCID.

---
class: impact

# ORCID Advantage: Reuse

---
class: img, orcid-reuse

![](./images/orcid-reuse.png)

---
# ORCID and Funding

As of Oct 2019, NIH will require all individuals supported by NIH training grants, fellowships, and career development awards to have ORCID iDs and they must be linked to their eRA Commons profiles.

https://grants.nih.gov/grants/guide/notice-files/NOT-OD-19-109.html

???
Increasingly funders are requiring or recommending use of ORCID.

As it will be required we can make them look good as another small way to improve our competitiveness for funding.

---
# [SciENcv (Science Expert Network Curriculum Vitae)](https://www.ncbi.nlm.nih.gov/sciencv/)<br>Biosketches for NIH & NSF

![](./images/sciencv-orcid-edited.png)

???
If you have an ORCID record populated it is one way to more easily complete NIH and NSF biosketches in SciENcv, which is the required format for these funders.

Research administrators can look out for trainings from the Libraries on using SciENcv.

---
class: impact
# How does someone find more of your work?

---
class: img
![](./images/royal-wiley-online.png)

???
https://onlinelibrary-wiley-com.prox.lib.ncsu.edu/doi/10.1111/evj.13042

---
class: img
![](./images/royal-wiley-pdf.png)

???
And even if someone gets to the work via a PDF the ORCID links are often there and the one way to quickly and easily get from the article to more information on the person.

---
class: img
![](./images/sheats-orcid-example.png)

---
class: impact
# ORCID and publishing
## Increase the discoverability of your work. Increase your impact.

---
class: img
![](./images/journal-of-vet-internal-medicine-submission.png)

???
ORCID is integrated into the workflow for article submission in different ways by different publishers.

Journal of Veterinary Internal Medicine has an option to login with ORCID for submission.

---
# Examples:
- Journal of Veterinary Internal Medicine
- Equine Veterinary Journal

> Wiley encourages all authors to register for an ORCID iD and associate it with their ScholarOne account. Many Wiley journals using ScholarOne also require submitting authors (only) to provide an ORCID iD when submitting a manuscript.

http://www.wileyauthors.com/orcid

???
Increasingly journals have requirements for authors to provide their ORCID iD when submitting.

---
# BMC Veterinary Research (Springer Nature)

> BMC's submission systems and publication workflows support the ORCiD iD (Open Researcher and Contributor ID) identifier, which is an ID that uniquely attaches your identity to your research work, such as your articles and datasets. The result: no more confusion because another researcher has the same or a similar name. You get the credit for your work.

> Whenever and wherever you publish—your ORCiD iD remains the same. You only need to register once.

https://www.biomedcentral.com/getpublished/orcid

---
# ORCID Tip: Auto-Updates

1. Use your ORCID iD when you publish.
2. Publisher pushes your citation with your associated ORCID iD to the Crossref DOI service.
3. Crossref pushes the citation to your ORCID record automatically.

You don't have to enter this data manually later if you use your ORCID iD early on in the publishing process.

???
To help keep your ORCID record

---
class: impact
## Use your ORCID iD when you publish.
Encourage your co-authors to use theirs too.

---
class: impact
# Local Context
???
So those are just a couple general reasons why you might want to use ORCID. I want to set some local context for why.

---
class: impact
# Citation Index

Collection of citations to works by NC State affiliated people.

## https://ci.lib.ncsu.edu

???
I want to introduce you to an application the Libraries has developed called the Citation Index.

[READ SLIDE]

---
class: impact
## Connect your ORCID iD with your Unity ID

## https://ci.lib.ncsu.edu & click "Get Started"

---
class: img
![](./images/ci-stewart.png)

???
When you make the connection between your ORCID iD your page displays your ORCID iD and a list of works we know about.

For this person we know of 10 works with 3 of them being open access.

We get the employment, education, and biography from ORCID and display those when available.

---
# ORCID + Citation Index

1. For works we know about we can push those citations to ORCID to help get your ORCID record started.
2. The Citation Index application can discover new works via ORCID.

???
But more importantly is that once you make that connection with your ORCID iD there are a couple of things that we can do for you.

[READ SLIDE]

And I'll talk now about why you might care about these.

---
# Local Auto-Updates

- Link ORCID iD to Unity ID with the Libraries through the Citation Index: https://ci.lib.ncsu.edu
- Whenever your ORCID record is updated...
- Libraries' Citation Index profile gets auto-updated with the citation from ORCID.
- Local systems can be updated.

???
And then we have the ability for you to turn on local auto-updates for your profile pages as well.

---
class: impact
# Digital Measures

Any citations we get in the Citation Index<br>we can provide for ingest into Digital Measures.<br><br>_(Development in progress.)_

???
In short any information we have on citations to works we can provide for ingest into Digital Measures.

This ought to save them a great deal of time from having to import and deduplicate citation data.

---
class: impact
# Save Researchers Time

???
The Libraries main goal in this work is to save researchers time from repeated data entry of citations.

---
class: impact
# Libraries Services

???
So I want to mention just one Libraries service to help save you more time.

---
# How does the Libraries get citation data?

1. Harvest citations from ORCID. (ORCID iD registered with the Libraries required.)
2. Harvest citations from [Web of Science](https://proxying.lib.ncsu.edu/index.php?url=http://www.webofknowledge.com/WOS) by affiliation. (Every week for the past 12+ years.)
3. Add citations to Citation Index based on a CV (or another list of citations) to fill in gaps. (Service provided by request.)

???
2. WoS: happens without further action by anyone.
3. CV service. I'll talk about next.

---
class: impact
# CV Service

???
So I want to talk more about the CV service we provide and how you can take advantage of it.

We're guessing many of you don't have ORCID records or don't have anything in them. And from Web of Science harvesting alone we have gaps.

---
# CV Service

- Want to help with the backlog of citation data entry for ORCID.
- Can use a CV (or another list of citations or link to citations) submitted to us.
- Convert citations into a machine-readable, reusable format.
- Deduplicate citations.
- Find DOIs to enhance data about works.
- Find links to open access copies.
- Jumpstart ORCID records by adding citations with DOIs to ORCID.

???

[READ SLIDE]

---
class: impact
# Getting Started with the CV Service

???
So how can you take advantage of this service?

---
# 5 Minutes to Register & Send a CV

1. Register ORCID iD with the Libraries. Can create ORCID iD as part of the process if don't already have one. Get started here: https://ci.lib.ncsu.edu
2. Send us your CV (or another list of citations or a link to a list of citations) with a short upload form. https://ci.lib.ncsu.edu/user/cv-service

That's it. We take it from there.

???
It takes 5 minutes or less.

1. To help you adopt ORCID we're asking you to register your ORCID iD with us and then we're backing it up with our services.

2. Once you register it is easy to send us your CV or another list of citations or a link to a list of your citations.

That's it. We take it from there.

---
# Results

For 4 DoCS faculty who linked their ORCID iD and sent us their CV we found 71 additional citations to fill in gaps for works we did not already know about from Web of Science or ORCID. For many we discovered DOIs.

---
class: impact
# Other Benefits of the Citation Index

---
## Clinical Sciences People

https://ci.lib.ncsu.edu/units/18/people

![](./images/ci-clinical-people.png)

???
Here's a page of all active Unity users associated with Clinical Sciences. The list begins with those who have already registered their ORCID iD so it is possible to see who has signed up already.

And with the "View as JSON" link you can get to a machine readable version of this page.

---
class: img
![](./images/ci-works-clinical.png)

???
We also have a view of unique works for the department for the year. In the bottom right is when we added the work to the index. Some use this for current awareness.

---
# Actions

1. Register with ORCID iD at: https://ci.lib.ncsu.edu
2. Send us your CV: https://ci.lib.ncsu.edu/user/cv-service
3. Use your ORCID iD when you publish.

---
class: impact
## https://ci.lib.ncsu.edu
## Click "Gest Started"

---
class: impact
# Registration

![](./images/ci-get-started.png)

---
# Link your Unity ID with your ORCID iD

![](./images/ci-create-or-connect.png)

---
class: img
![](./images/ci-orcid-sign-in.png)

---
class: img
![](./images/ci-orcid-authorize.png)

---
class: impact

# Citation Index Dashboard

## https://ci.lib.ncsu.edu/user

---
# Next Steps

## 1. Send us your CV<br>(or another list of your publications)

https://ci.lib.ncsu.edu/user/cv-service

## 2. Use your ORCID iD when you submit for publication

---
# Contact

Jason Ronallo: jnronall@ncsu.edu

Help: group-lib-citations@ncsu.edu

Get Started: https://ci.lib.ncsu.edu

---
# CV Service Caveats

1. Libraries data may be incomplete or behind. Only as good as what we're able to harvest or get from researchers.
2. Can commit to an initial push this fall with the CV service to add citations for all of CVM going back 3 years. Rest of citations worked as part of backlog as time allows.

???
So I want to state some caveats.

1. Even for someone who comes through our service, Libraries data may be incomplete or not current. It is only as good as what we're able to harvest or get from the researchers. I've got a solution for this one.
2. We've got a lot of past citations which could be added. What the Libraries could agree to support is going back 3 years to support initial adoption of Digital Measures. Then as we have the capacity we can work back further.
