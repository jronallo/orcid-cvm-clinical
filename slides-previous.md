title: CVM Citations
class: animation-fade
layout: true

.bottom-bar[
  https://ronallo.com/presentations/orcid-cvm
]

---
class: impact
# CVM Citations

## https://ronallo.com/presentations/orcid-cvm
## Press "p" to see speaker notes.

## Get Started: https://ci.lib.ncsu.edu

???
If you'd like, these slides have speaker notes on them.

---
class: impact

# Click Get Started:
## https://ci.lib.ncsu.edu

Get access to Libraries services to help maintain your citations in [ORCID](https://orcid.org).

???
So we're going to help get you signed up and taking advantage of some Libraries services.

You can get started here now at ci.lib.ncsu.edu

---
# Jason Ronallo
## NCSU Libraries
## Department Head of Digital Library Initiatives

jnronall@ncsu.edu

https://www.lib.ncsu.edu/staff/jnronall

## Get Started: https://ci.lib.ncsu.edu

???
We work with a range of different technologies to provide novel solutions to library and archives challenges.

Increasingly we're looking for how to support the research mission of the university.


---
# Libraries Services

- Harvest citations from Web of Science by affiliation.
- Add citations to Citation Index based on your CV (or another list of citations) to fill in gaps.
- Convert citations into a machine-readable, reusable format.
- Find DOIs to enhance data.
- Find links to open access copies.
- Jumpstart ORCID records by adding citations with DOIs.

Sign up and send us your CV to get started: https://ci.lib.ncsu.edu

???
To help you adopt ORCID we're asking you to sign up with us and then we're backing it up with all kinds of services.

We're especially looking to support how to set up a profile for auto-updates and how to import existing citations into ORCID to have a fuller profile.

---
class: img
![](./images/directory-jennings-top.png)

???
So here's briefly why you may be interested.

This is one of the directory pages from your department.

https://ci.lib.ncsu.edu/profiles/kmjennin

---
class: img
![](./images/directory-jennings-bottom.png)

???
If you scroll down the page you'll see more publications and a link to view all publications.

---
class: img
![](./images/ci-jennings.png)

???
Which leads to a page on the Libraries website.

You may not have known that the citations which show up on some directory pages come from the Libraries.

---
class: img
![](./images/ci-jennings-citation.png)

???
If we look at one of the citations you can see various features:

- Export a citation
- Click through to other NC State authors
- Prominently display DOIs
- Automatically link to open access copies when available to better boost access to your work

---
## https://ci.lib.ncsu.edu/units/17

![](./images/ci-works-hs.png)

???
We also provide a page for departments which list the recent publications we know about.

And what I'm showing now is what the new interface will look like. So if you go there right now it won't look exactly like this.

The caveat is that we don't know about all publications but we hope that between different sources we can get closer and represent some of the works that don't make it into the big indexes.

---
## https://ci.lib.ncsu.edu/units/17/people

![](./images/ci-people-hs.png)

???
And you can also see an overview of people associated with the department, how many works we know that they've authored, and a link to their ORCID iD if they registered on the site.

---
class: impact
# Citation Index
# [ci.lib.ncsu.edu](https://ci.lib.ncsu.edu)

???
OK. So now we're going to get you signed up so we can help make your profile pages more current.

So in order to update your profile we ask that you register your ORCID iD with us.

---
# Sign Up Overview
1. Link your Unity ID with your ORCID iD: https://ci.lib.ncsu.edu
2. Send us your CV: https://ci.lib.ncsu.edu/user/cv-service

Use your ORCID iD when you publish. Your ORCID record, Citation Index record, and directory page will all be updated automatically.

???
Here's what we're going to try to accomplish in the next several minutes.

---
# Sign Up Steps
1. Visit [ci.lib.ncsu.edu](https://ci.lib.ncsu.edu)
2. Click "Get Started"
3. Login with your Unity ID via Shibboleth
4. Click on the "Create or connect your ORCID iD button"
5. Follow the steps in the ORCID pop-up window
  - If you already have an ORCID iD then login to ORCID
  - Or click to "Register now" to create an ORCID iD
6. Click "Authorize" to connect your Unity ID with ORCID iD

Upload your CV: https://ci.lib.ncsu.edu/user/cv-service

---
# Create or connect button

![](./images/ci-create-or-connect-button.png)

Detailed visual instructions:<br>
http://ronallo.com/ci-tutorial/connect-orcid/click-create-or-connect-button/

---
class: cv-service

# Submit your CV
# https://ci.lib.ncsu.edu/user/cv-service

## We will:
- Fill gaps in your Citation Index profile
- Look for DOIs
- Look for open access copies
- Add works with DOIs to your ORCID record

---
class: img
![](./images/orcid-jennings.png)

???
You then may see citations in your ORCID record with a source of "NC State University Libraries" like these.

---
# Next Steps
- Use your ORCID iD when you publish
- Contact us if you need help or have questions:<br> group-lib-citations@ncsu.edu
- We'll email you with an update on progress when we've added the most recent citations from your CV.

---
class: impact

# What did we just do?

---
class: impact

# How do other researchers find you?

???
I want to start with this question.

---
class: impact
## When someone finds an article you authored,<br>how do they find your other works?


---
class: img
![](./images/horticulture-research.png)

???
One way is that they discover an article of interest.

---
class: img
![](./images/horticulture-research-ashrafi.png)

???
If they click on one of the names they can see your ORCID iD and learn more about your work.

---
# Nature Author Guidelines

> As part of our efforts to improve transparency and unambiguous attribution of scholarly contributions, corresponding authors of published papers must provide their Open Researcher and Contributor Identifier (ORCID) iD; co-authors are encouraged to provide ORCiD iDs.

https://www.nature.com/authors/policies/authorship.html

More information on ORCID from Springer Nature including their ORCID mandate trial:
https://www.springernature.com/gp/researchers/orcid

???
So first, how does that link get on the article page?

Publishers are beginning to require the submitting author have an ORCID iD and recommending other others use ORCID as well.

---
# ORCID iD
- Persistent identifier for a researcher.
- Helps with name disambiguation. Uniquely identifies you.
- Record for associating research outputs like published works and funding history.
- You are in control.
- Follows a researcher through their whole career.
- Integrates with hundreds of systems.

???
In case you're not familiar already, here's a bit about what ORCID provides.

[READ SLIDE]

---
class: impact
## https://orcid.org/0000-0003-3708-3973
## ORCID iD for Katie Jennings

???
And here's what an ORCID identifier looks like. It is just a URL with a UUID at the end.

---
# What can you add to an ORCID record?

- **Works** (journal articles, book chapters, conference papers, etc.)
- Employment
- Education
- Biographical information including AKA for name changes, country, emails, other identifiers
- Links to your lab or personal websites
- Resources you use for your research like lab facilities or specialized equipment
- Peer review activity
- Invited positions, distinctions, membership, service
- Funding

???
You can represent much of your research output that matters to other researchers and funders in ORCID.

The Libraries is starting by helping with adding works to ORCID.

---
class: impact
# ORCID Members
## Libraries and Office of Research & Innovation

https://orcid.org

???
ORCID is a membership organization. The Libraries is a member in partnership with the Office of Research and Innovation.

---
class: impact
# ORCID's Advantage:
## Integrations with the ORCID API
More data reuse<br>across many systems<br>saves researchers time.

???
So that's just a bit about what ORCID provides.

We think the whole ecosystem supporting an open identifier like ORCID begins to provide mutual benefits all around.

This is one of the main differentiators from other profiles and records of your work. 100s of other systems integrate with ORCID.

Since ORCID is in common use and has an API, it means that it has a lot more potential to save researchers time, especially when compared to what we're currently doing.

---
class: impact
# Auto-Updates
## Reduce data entry.

???
Now I'd like to explain one way in which ORCID can save you time through an integration.

It is a feature called auto-updates. Here's how it works.

---
# Auto-Updates
1. Use your ORCID iD when you publish.
2. Publisher pushes your citation with your associated ORCID iD to the Crossref DOI service.
3. Crossref pushes the citation to your ORCID profile automatically.

You save time.

???
[READ slide]

This means that researchers don't have to enter this data by hand later if they use the identifier early on in the process.

---
# Local Auto-Updates

- Link ORCID iD to Unity ID: https://ci.lib.ncsu.edu
- Whenever your ORCID record is updated...
- Libraries' Citation Index profile gets auto-updated with the citation from ORCID.
- _Horticultural Science department profile pages get auto-updated via the Libraries' API._

???
And we have the ability for you to turn on local auto-updates for your profile pages as well.






---
class: impact
# Questions?

## Click Get Started: [ci.lib.ncsu.edu](https://ci.lib.ncsu.edu)



---
# Contact

Jason Ronallo: jnronall@ncsu.edu

Help: group-lib-citations@ncsu.edu

https://ci.lib.ncsu.edu
