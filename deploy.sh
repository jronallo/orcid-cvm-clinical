#!/usr/bin/env bash

/home/jnronall/code/backslide-jronallo/bin/bs e --media

# /home/jnronall/code/tmp/backslide-jronallo/bin/bs p

ssh ronallo "mkdir /var/www/presentations/orcid-cvm-docs"

rsync -avz dist/slides.html ronallo:/var/www/presentations/orcid-cvm-docs/index.html

exo-open https://ronallo.com/presentations/orcid-cvm-docs/
